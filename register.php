<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/styles.css">

        <title>Home - Bunny Beans</title>
    </head>
    <body>


      <?php require("includes/header.php"); ?>

        <main class="l-main">
            <section class="section section  bd-container" >
                <div class="loginSignUp-bg">
                    <form class="form" action="">
                        <h2 class="section-title contact__initial text-center">Register</h2>
                        <div class="form-control" id="message"></div>
                        <div class="form-control">
                            <input type="text" placeholder="Full Name" name="name">
                        </div>
                        <div class="form-control">
                            <input type="email" placeholder="Email Address" name="email">
                            <small></small>
                        </div>
                        <div class="form-control">
                            <input type="text" placeholder="Phone No(eg.03325856744)" name="phone">
                        </div>
                        <div class="form-control">
                            <input type="text" placeholder="Password" name="password">
                        </div>
                        <div class="form-control">
                            <input type="text" placeholder="Confirm Password" name="confirm_password">
                        </div>
                        <div class="form-control">
                            <button type="submit" class="button">Register Now</button>
                        </div>
                    </form>
                </div>
            </section>
            
          
        </main>
        <?php require "includes/footer.php";
            require "includes/scripts.php";
         ?>
  
    </body>
</html>