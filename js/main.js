$(function(){
    const selectedTheme = localStorage.getItem('selected-theme');
    if(selectedTheme=="dark"){
            $("body").addClass("dark-theme");
    }
    $("#theme-button").on("click",function(){
        if($("body").hasClass("dark-theme")){
            $("body").removeClass("dark-theme");
            localStorage.setItem('selected-theme', "light");
        }else{
            $("body").addClass("dark-theme");
            localStorage.setItem('selected-theme', "dark");
        }
    });

    $(window).scroll(function(){
        if($(this).scrollTop() >= 560){ $("#scroll-top").addClass('show-scroll');
        }else{ $("#scroll-top").removeClass('show-scroll');}
    });


    


});