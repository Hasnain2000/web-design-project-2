  <header class="l-header" id="header">
            <nav class="nav bd-container">
                <a href="#" class="nav__logo"><img class="logo" src="graphics/logo.png" alt="logo" ></a>

                <div class="nav__menu" id="nav-menu">
                    <ul class="nav__list">
                        <li class="nav__item">
                            <a href="#home" class="nav__link">Home</a>
                        </li>
                        <li class="nav__item">
                            <a href="#services" class="nav__link">Menu</a>
                        </li>
                        <li class="nav__item">
                            <a href="#about" class="nav__link">About</a>
                        </li>
                        <li class="nav__item">
                            <a href="#contact" class="nav__link">Contact us</a>
                        </li>
                        <li class="nav__item">
                            <a href="" class="cart-button"><img src="graphics/icon/cart-icon.svg"></a>
                        </li>
                        <li>
                            <img src="graphics/icon/moon-icon.svg" class='change-theme' id="theme-button">
                        </li>
                    </ul>
                </div>

                <div class="nav__toggle" id="nav-toggle">&#9776;</div>
            </nav>
        </header>