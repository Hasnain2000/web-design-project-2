<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/styles.css">

        <title>Home - Bunny Beans</title>
    </head>
    <body>


      <?php require("includes/header.php"); ?>

        <main class="l-main">
            <section class="section section  bd-container" >
                <div class="loginSignUp-bg">
                    <form class="form" action="">
                        <h2 class="section-title contact__initial text-center">Sign In</h2>
                        <div class="form-control">
                            
                        </div>
                        <div class="form-control">
                            <input type="text" placeholder="Email Address" name="">
                        </div>
                        <div class="form-control">
                            <input type="text" placeholder="Password" name="">
                        </div>
                        <div class="form-control">
                            <a href="">Forgot Password?</a>
                        </div>
                        <div class="mb-10 form-control">
                            <button type="submit" class="button">Sign In</button>
                        </div>
                        <div class="mb-10 text-center">
                            or
                        </div>
                        <div class="text-center">
                            <a href="register.php" class="link">Create a Account</a>
                        </div>  
                    </form>
                </div>
            </section>
            
          
        </main>
        <?php require "includes/footer.php";
            require "includes/scripts.php";
         ?>
  
    </body>
</html>